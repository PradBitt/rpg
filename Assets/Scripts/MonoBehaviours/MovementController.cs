﻿using System.Collections;
using UnityEngine;

public class MovementController : MonoBehaviour
{

    public float movementSpeed = 3.0f;
    Vector2 movement = new Vector2();
    private bool attacking = false;

    // Physics component
    Rigidbody2D rb2D;
    // Animator component
    Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        rb2D = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        animator.SetFloat("yDir", -1);
    }

    // Update is called once per frame
    void Update()
    {
        UpdateState();
    }

    void FixedUpdate()
    {
        MoveCharacter();
    }

    private void MoveCharacter()
    {
        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");



        movement.Normalize();

        rb2D.velocity = movement * movementSpeed;
    }

    void UpdateState()
    {

        if (Input.GetKeyDown(KeyCode.Space) && !animator.GetBool("isHitting"))
            StartCoroutine("HitCo");
            
        if (Mathf.Approximately(movement.x, 0) && Mathf.Approximately(movement.y, 0))
            animator.SetBool("isWalking", false);
        else
        {
            animator.SetBool("isWalking", true);
            animator.SetFloat("xDir", movement.x);
            animator.SetFloat("yDir", movement.y);
        }

    }

    IEnumerator HitCo()
    {
        animator.SetBool("isHitting", true);
        yield return new WaitForSeconds(0.3f);
        animator.SetBool("isHitting", false);
    }
}
