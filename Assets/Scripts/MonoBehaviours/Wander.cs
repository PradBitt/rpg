﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(CircleCollider2D))]
[RequireComponent(typeof(Animator))]
public class Wander : MonoBehaviour
{
    public float persuitSpeed;
    public float wanderSpeed;
    float currentSpeed;

    public float directionChangeInterval;
    public bool followPlayer;
    Coroutine moveCoroutine;
    Rigidbody2D rb2d;
    Animator animator;

    Transform targetTransform = null;
    Vector3 endPosition;

    // Gizmos
    CircleCollider2D circleCollider;


    private void Start()
    {
        animator = GetComponent<Animator>();
        currentSpeed = wanderSpeed;
        rb2d = GetComponent<Rigidbody2D>();
        StartCoroutine(WanderRoutine());
        circleCollider = GetComponent<CircleCollider2D>();
    }

    private void Update()
    {
        Debug.DrawLine(transform.position, endPosition, Color.red);
    }

    public IEnumerator WanderRoutine()
    {
        while(true){
            ChooseNewEndpoint();
            if(moveCoroutine != null)
            {
                StopCoroutine(moveCoroutine);
            }
            moveCoroutine = StartCoroutine(Move());
            yield return new WaitForSeconds(directionChangeInterval);
        }
    }

    private void ChooseNewEndpoint()
    {
        float randomAngle = Mathf.Deg2Rad * Random.Range(0,360);
        endPosition =  new Vector3(transform.position.x+2*Mathf.Cos(randomAngle), transform.position.y + 2*Mathf.Sin(randomAngle));
    }

    IEnumerator Move()
    {
        rb2d.velocity = Vector3.zero;
        float remainingDistance = (transform.position - endPosition).sqrMagnitude;
        while(remainingDistance > 0.001)
        {
            if(targetTransform != null)
            {
                endPosition = targetTransform.position;
            }

            if(rb2d != null)
            {
                animator.SetBool("isWalking",true);
                Vector3 newPosition = Vector3.MoveTowards(transform.position, endPosition, currentSpeed * Time.deltaTime);
                rb2d.MovePosition(newPosition);
                remainingDistance = (transform.position - endPosition).sqrMagnitude;
            }
            yield return new WaitForFixedUpdate();
        }

        animator.SetBool("isWalking", false);

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(followPlayer && collision.gameObject.CompareTag("Player"))
        {
            currentSpeed = persuitSpeed;
            targetTransform = collision.gameObject.transform;
            if (moveCoroutine != null)
                StopCoroutine(moveCoroutine);
            moveCoroutine = StartCoroutine(Move());
        }

    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (followPlayer && collision.gameObject.CompareTag("Player"))
        {
            animator.SetBool("isWalking", false);
            if (moveCoroutine != null)
                StopCoroutine(moveCoroutine);
            currentSpeed = wanderSpeed;
            targetTransform = null;
        }
    }

    private void OnDrawGizmos()
    {
        if(circleCollider != null)
        {
            Gizmos.DrawWireSphere(transform.position, circleCollider.radius);
        }
    }

}
